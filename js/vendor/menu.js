(function($) {
  $(function() {
    $('nav .navbar-nav .dropdown> a:not(:only-child)').click(function(e) {
      $(this).siblings('.dropdown-menu').toggle();
      $('').not($(this).siblings()).hide();
      e.stopPropagation();
    });
    $('html').click(function() {
      $('.dropdown-menu').hide();
    });
  });
})
(jQuery);
